FROM node:14-slim

# make the 'app' folder the current working directory
WORKDIR /app

RUN apt update && apt install g++ build-essential git -y

RUN npm install -g @vue/cli; \
    sed -i 's/subscriptionsPath/host: "0.0.0.0", subscriptionsPath/' /usr/local/lib/node_modules/@vue/cli/lib/ui.js

# expose these ports
EXPOSE 8000 8080

# copy both 'package.json' and 'package-lock.json' (if available)
COPY ./package*.json ./

RUN npm install

COPY . .

CMD ["sh", "./scripts/entrypoint.sh" ]
