import Vue from "vue"
import Router from "vue-router"
import CVPage from "./views/CVPage"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "main",
      component: CVPage
    },
    {
      path: "/cv",
      name: "cv",
      component: CVPage
    }
  ]
})
