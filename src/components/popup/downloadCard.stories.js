import DownloadCard from "@/components/popup/downloadCard"

export default {
  title: "CV/popup/DownloadCard",
  components: { DownloadCard }
}

const Template = (args, { argTypes }) => ({
  template: `
  <DownloadCard>

    <div style="height: 200px; width: 100px">
    <p>container</p>
    </div>
  </DownloadCard>
  `,
  components: { DownloadCard },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
