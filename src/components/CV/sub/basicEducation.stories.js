import BasicEducation from "@/components/CV/sub/basicEducation.vue"

export default {
  title: "CV/BasicEducation",
  components: { BasicEducation }
}

const dataExample = {
  "Master Electromechanics": {
    startdate: "2015",
    enddate: "2017",
    degree: "Master of Science: Master Industrial Engineer Electromechanics",
    school: "KU Leuven University",
    place: "Gent Belgium"
  },
  "Master Erasmus": {
    startdate: "jan 2016",
    enddate: "mei 2016",
    degree: "Master industrial ingenieur, Erasmus",
    school: "Halmstad University",
    place: "Halmstad Sweden",
    courses: "Robotic manipulations and Intelligent vehicles"
  },
  "Transition Electromechanics": {
    startdate: "2013",
    enddate: "2015",
    degree: "Transition year Industrial Engineer Electromechanics",
    school: "KU Leuven University",
    place: "Gent Belgium"
  },
  "Bachelor Ecletromechanics": {
    startdate: "2010",
    enddate: "2013",
    degree: "Professional Bachelor of Electromechanics",
    school: "Hogeschool Gent College",
    place: "Gent Belgium"
  },
  "highschool technical": {
    startdate: "2006",
    enddate: "2010",
    degree: "Electromechanics",
    school: "VTI r secondary education",
    place: "Roeselare Belgium"
  },
  "highschool general": {
    startdate: "2004",
    enddate: "2006",
    degree: "General secondary education",
    school: "Broederschool secondary education",
    place: "Roeselare Belgium"
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <basicEducation
    :dataList="educationList"
    :title="title"
  />
  `,
  components: { BasicEducation },
  props: Object.keys(argTypes)
})

export const Main = Template.bind({})
Main.args = {
  educationList: {
    "Master Electromechanics": {
      startdate: "2015",
      enddate: "2017",
      degree: "Master of Science: Master Industrial Engineer Electromechanics",
      school: "KU Leuven University",
      place: "Gent Belgium"
    }
  },
  title: "Education"
}

export const Full = Template.bind({})
Full.args = {
  educationList: dataExample,
  title: "Education"
}
