import Vue from "vue"
import Vuex from "vuex"
import Links from "@/components/CV/sub/links"

import { getBaseUrl } from "@/utils/base_url.js"
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    baseUrl: getBaseUrl()
  },
  getters: {
    getBaseUrl: (state) => {
      return state.baseUrl
    }
  }
})

export default {
  title: "CV/Links",
  components: { Links },
  excludeStories: /.*store$/
}

const Template = (args, { argTypes }) => ({
  template: `
  <links
    :dataList="dataList"
    :height="height"
  />
  `,
  components: { Links },
  props: Object.keys(argTypes),
  store
})

export const Main = Template.bind()
Main.args = {
  dataList: {
    linkedin: {
      url: "https://www.linkedin.com/in/bastiaan-van-denabeele-389507b3/",
      text: "LinkedIn",
      icon: "/icons/LinkedIn/logo.svg"
    }
  },
  height: 10
}
