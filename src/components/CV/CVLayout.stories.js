import Vue from "vue"
import Vuex from "vuex"
import CVLayout from "@/components/CV/CVLayout"

import data from "@/assets/resume/data.json"
import { getBaseUrl } from "@/utils/base_url.js"

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    resume_data: data,
    baseUrl: getBaseUrl(),
    A4Layout: {
      height: 1754,
      width: 1240,
      padding: 24
    }
  },
  getters: {
    getresume: (state) => {
      return state.resume_data
    },
    getresumeEN: (state) => {
      return state.resume_data.cv_eng
    },
    getBaseUrl: (state) => {
      return state.baseUrl
    },
    getA4Layout: (state) => {
      return state.A4Layout
    }
  }
})

export default {
  title: "CV/CVLayout",
  components: { CVLayout },
  excludeStories: /.*store$/
}

const Template = (args, { argTypes }) => ({
  template: `
  <CVLayout/>
  `,
  components: { CVLayout },
  props: Object.keys(argTypes),
  store
})

export const Main = Template.bind()
