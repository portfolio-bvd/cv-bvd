import ContentSectionCard from "@/components/atoms/ContentSectionCard"
import ContentList from "@/components/molecules/ContentList"
import EducationListItem from "@/components/molecules/EducationListItem"
import ExperienceListItem from "@/components/molecules/ExperienceListItem"
import ExperienceGroupListItem from "@/components/molecules/ExperienceGroupListItem"
import PersonaliaListItem from "@/components/molecules/PersonaliaListItem"
import FlexColumn from "@/components/molecules/FlexColumn"
import Skill from "@/components/molecules/Skill"

export default {
  title: "CV/Templates/SectionCards",
  components: { ContentSectionCard },
  subcomponents: { ContentList, EducationListItem }
}

export const EducationCard = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ContentSectionCard, ContentList, EducationListItem },
  template: `
  <ContentSectionCard title="Education" :border="true">
    <ContentList :separator="true">
      <EducationListItem
        v-for="item in items"
        :key="item.id"
        :degree="item.degree"
        :school="item.school"
        :location="item.location"
        :courses="item.courses"
        :startdate="item.startdate"
        :enddate="item.enddate"
      />
    </ContentList>
  </ContentSectionCard>
  `
})

EducationCard.args = {
  items: [
    {
      id: 1,
      degree: "Master of ...",
      school: "University",
      location: "Neverland",
      courses: "fun",
      startdate: "2018-09-06T13:07:04.054",
      enddate: "2019-09-06T13:07:04.054"
    },
    {
      id: 2,
      degree: "Master of something",
      school: "University",
      location: "Neverland",
      startdate: "2018-10-06T13:07:04.054",
      enddate: "2019-12-06T13:07:04.054"
    }
  ]
}

export const SkillCard = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ContentSectionCard, FlexColumn, Skill },
  template: `
  <ContentSectionCard title="Skill" :border="false">
    <FlexColumn>
      <Skill
        v-for="item in items"
        :key="item.id"
        :name="item.name"
        :level="item.level"
      />
    </FlexColumn>
  </ContentSectionCard>
  `
})
SkillCard.args = {
  items: [
    { id: 1, level: 0.8, name: "Python" },
    { id: 2, level: 0.5, name: "Javascript" }
  ]
}

export const PersonaliaCard = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ContentSectionCard, ContentList, PersonaliaListItem },
  template: `
  <ContentSectionCard title="Personalia" :border="false">
    <ContentList :separator="true">
      <PersonaliaListItem
        v-for="item in items"
        :key="item.id"
        :text="item.text"
        :icon="item.icon"
      />
    </ContentList>
  </ContentSectionCard>
  `
})
PersonaliaCard.args = {
  items: [
    {
      id: 1,
      text: "Bastiaan Van denabeele",
      info: "name",
      icon: "person"
    },
    {
      id: 2,
      text: "Belgian",
      info: "nationality",
      icon: "flag"
    },
    {
      id: 3,
      text: "Barcelona",
      info: "living",
      icon: "home"
    },
    {
      id: 4,
      text: "vandenabeele.bastiaan<wbr>@gmail.com",
      info: "email",
      icon: "email"
    }
  ]
}

export const ExperienceCard = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ContentSectionCard, ContentList, ExperienceListItem },
  template: `
  <ContentSectionCard title="Experience" :border="true">
    <ContentList :separator="true">
      <ExperienceListItem
        v-for="item in items"
        :key="item.id"
        :company="item.company"
        :title="item.title"
        :about="item.about"
        :link="item.link"
        :location="item.location"
        :tasks="item.tasks"
        :startdate="item.startdate"
        :enddate="item.enddate"
      />
    </ContentList>
  </ContentSectionCard>
  `
})
ExperienceCard.args = {
  items: [
    {
      id: 1,
      startdate: "2018-10-06T13:07:04.054",
      enddate: "now",
      company: "OneRagtime",
      about: "Venture capitalist fund who invests in tech by utilizing tech",
      title: "Lead developer",
      link: "https://www.oneragtime.com/",
      location: "Barcelona Spain",
      tasks: [
        "Maintaining , refactoring and debugging existing code base",
        "Introducing best practices: Testing ,Clean code, Refactoring, Agile, Scrum",
        "Technical breakdown and development of new features",
        "Supporting business and product team in planning",
        "Leading and mentoring the developer team",
        "Performing tech duediligent in potential investable startups"
      ]
    },
    {
      id: 2,
      startdate: "2018-10-06T13:07:04.054",
      enddate: "2019-12-06T13:07:04.054",
      company: "BCG / Kernel analytics",
      about: "Develop data driven web applications",
      title: "Full-stack web developer",
      link: "https://www.bcg.com/beyond-consulting/bcg-gamma/default.aspx",
      location: "Barcelona Spain",
      tasks: [
        "Develop data driven web applications, with the data science team",
        "Communicate with the business team in planning",
        "Refactor and develop new feature on legacy code bases",
        "Write documentation"
      ]
    }
  ]
}

export const MultiExperienceCard = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: {
    ContentSectionCard,
    ContentList,
    ExperienceListItem,
    ExperienceGroupListItem
  },
  template: `
  <ContentSectionCard title="Experience" :border="true">
    <ContentList :separator="true">
      <ExperienceGroupListItem
        v-for="experience in experiences"
        :groupTitle="experience.name" 
      >
        <ContentList >
          <ExperienceListItem
            v-for="item in experience.items"
            :key="item.id"
            :company="item.company"
            :title="item.title"
            :about="item.about"
            :link="item.link"
            :location="item.location"
            :tasks="item.tasks"
            :startdate="item.startdate"
            :enddate="item.enddate"
          />
        </ContentList>
      </ExperienceGroupListItem>
    </ContentList>
  </ContentSectionCard>
  `
})
MultiExperienceCard.args = {
  experiences: [
    {
      id: 1,
      name: "Professional experience",
      items: [
        {
          id: 1,
          startdate: "2018-10-06T13:07:04.054",
          enddate: "2019-12-06T13:07:04.054",
          company: "OneRagtime",
          about:
            "Venture capitalist fund who invests in tech by utilizing tech",
          title: "Lead developer",
          link: "https://www.oneragtime.com/",
          location: "Barcelona Spain",
          tasks: [
            "Maintaining , refactoring and debugging existing code base",
            "Introducing best practices: Testing ,Clean code, Refactoring, Agile, Scrum",
            "Technical breakdown and development of new features",
            "Supporting business and product team in planning",
            "Leading and mentoring the developer team",
            "Performing tech duediligent in potential investable startups"
          ]
        },
        {
          id: 2,
          startdate: "2018-10-06T13:07:04.054",
          enddate: "2019-12-06T13:07:04.054",
          company: "BCG / Kernel analytics",
          about: "Develop data driven web applications",
          title: "Full-stack web developer",
          link: "https://www.bcg.com/beyond-consulting/bcg-gamma/default.aspx",
          location: "Barcelona Spain",
          tasks: [
            "Develop data driven web applications, with the data science team",
            "Communicate with the business team in planning",
            "Refactor and develop new feature on legacy code bases",
            "Write documentation"
          ]
        }
      ]
    },
    {
      id: 2,
      name: "Other experiences",
      items: [
        {
          id: 1,
          startdate: "2018-10-06T13:07:04.054",
          enddate: "2019-12-06T13:07:04.054",
          company: "multiple companies",
          about: "various summer jobs",
          tasks: ["Electrical wiring", "Welding", "Machine operating"]
        },
        {
          id: 2,
          startdate: "2018-10-06T13:07:04.054",
          enddate: "2019-12-06T13:07:04.054",
          company: "Kazou",
          title: "Head animator",
          location: "Europe",
          tasks: [
            "Responsible for leading summer camp with childeren from 11-16",
            "Animating summer camps for participants with disabilities 16-30"
          ]
        }
      ]
    }
  ]
}
