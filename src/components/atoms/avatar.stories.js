import Avatar from "@/components/atoms/avatar"

export default {
  title: "CV/Atoms/Avatar",
  components: { Avatar }
}

export const Main = (args, { argTypes }) => ({
  template: "<avatar/>",
  components: { Avatar },
  props: Object.keys(argTypes)
})
