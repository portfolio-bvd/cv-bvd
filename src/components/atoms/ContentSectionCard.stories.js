import ContentSectionCard from "@/components/atoms/ContentSectionCard"

export default {
  title: "CV/Atoms/ContentSectionCard",
  components: { ContentSectionCard },
  args: {
    title: "Person Name"
    //border: true
  },
  argTypes: {
    border: {
      name: "border",
      type: { name: "boolean", required: false },
      defaultValue: true,
      description: "border around card"
    }
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <ContentSectionCard
    :title="title"
    :border="border"
  />
  `,
  components: { ContentSectionCard },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
