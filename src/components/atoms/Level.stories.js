import Level from "@/components/atoms/Level"

export default {
  title: "CV/atoms/Level",
  components: { Level }
}

const Template = (args, { argTypes }) => ({
  template: `
  <div style="width: 200px">
    <Level
      :level="level"
    />
  </div>
  `,
  components: { Level },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  level: 0.6
}
