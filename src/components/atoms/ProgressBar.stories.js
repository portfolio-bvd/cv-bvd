import ProgressBar from "@/components/atoms/ProgressBar"

export default {
  title: "CV/atoms/ProgressBar",
  components: { ProgressBar },
  argTypes: {
    percent: {
      name: "percent",
      type: { name: "number", required: false },
      description: "percentage of the progress"
    }
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <div style="width:200px">
  <ProgressBar
    :percent="percent"
  >
  </div>
  `,
  components: { ProgressBar },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
