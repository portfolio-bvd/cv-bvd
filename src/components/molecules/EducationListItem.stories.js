import EducationListItem from "@/components/molecules/EducationListItem"
const argTypes = {
  degree: {
    name: "degree",
    type: { name: "string", required: true },
    defaultValue: "Master in Spaceship Thinking",
    description: "name of the degree",
    control: {
      type: "text"
    }
  },
  school: {
    name: "school",
    type: { name: "string", required: true },
    defaultValue: "University of Moon",
    description: "name of the school where the degree was completed",
    control: {
      type: "text"
    }
  },
  courses: {
    name: "courses",
    type: { name: "string", required: false },
    defaultValue: "Watching the stars",
    description: "optional courses taken in education",
    control: {
      type: "text"
    }
  },
  location: {
    name: "location",
    type: { name: "string", required: true },
    defaultValue: "Battleship",
    description: "the city and country where the degree was followed",
    control: {
      type: "text"
    }
  },
  startdate: {
    name: "startdate",
    type: { name: "string", required: true },
    defaultValue: "2018-08-06T13:07:04.054",
    description: "start date",
    control: {
      type: "date"
    }
  },
  enddate: {
    name: "enddate",
    type: { name: "string", required: true },
    defaultValue: "2019-09-06T13:07:04.054",
    description: "end date",
    control: {
      type: "date"
    }
  }
}

export default {
  title: "CV/Molecules/EducationListItem",
  components: { EducationListItem },
  argTypes: {
    ...argTypes
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <educationListItem
    :degree="degree"
    :school="school"
    :location="location"
    :courses="courses"
    :startdate="startdate"
    :enddate="enddate"
  />
  `,
  components: { EducationListItem },
  props: Object.keys(argTypes)
})

export const FullItemList = Template.bind({})
export const WithoutCourse = Template.bind({})
WithoutCourse.args = {
  degree: "Master Jedi",
  shool: "The Jedi Temple",
  location: "far far away",
  startdate: "2018-09-06T13:07:04.054",
  enddate: "2019-09-06T13:07:04.054",
  courses: undefined
}
