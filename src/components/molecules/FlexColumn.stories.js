import FlexColumn from "@/components/molecules/FlexColumn"
import Skill from "@/components/molecules/Skill"

export default {
  title: "CV/Molecules/FlexColumn",
  components: { FlexColumn },
  subcomponents: { Skill }
}

export const flexColumn = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { FlexColumn, Skill },
  template: `
    <FlexColumn>
      <Skill
        v-for="item in items"
        :key="item.id"
        :name="item.name"
        :level="item.level"
      />
    </FlexColumn>
  `
})
flexColumn.args = {
  items: [
    { id: 1, level: 0.8, name: "python" },
    { id: 2, level: 0.5, name: "Javascript" }
  ]
}
