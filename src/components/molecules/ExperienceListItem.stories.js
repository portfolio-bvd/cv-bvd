import ExperienceListItem from "@/components/molecules/ExperienceListItem"

const argTypes = {
  company: {
    name: "company",
    type: { name: "string", required: true },
    defaultValue: "Fake Google",
    description: "name of the company",
    control: {
      type: "text"
    }
  },
  title: {
    name: "title",
    type: { name: "string", required: true },
    defaultValue: "Developer",
    description: "Job title",
    control: {
      type: "text"
    }
  },
  about: {
    name: "about",
    type: { name: "string", required: false },
    defaultValue: "Company does fun stuff",
    description: "Makes software",
    control: {
      type: "text"
    }
  },
  location: {
    name: "location",
    type: { name: "string", required: true },
    defaultValue: "Neverland",
    description: "the city and country where the degree was followed",
    control: {
      type: "text"
    }
  },
  tasks: {
    name: "tasks",
    type: { name: "tasks", required: true },
    defaultValue: ["first tasks", "second"],
    description: "task done in the company",
    control: {
      type: "array"
    }
  },
  startdate: {
    name: "startdate",
    type: { name: "string", required: true },
    defaultValue: "2018-08-06T13:07:04.054",
    description: "start date",
    control: {
      type: "date"
    }
  },
  enddate: {
    name: "enddate",
    type: { name: "string", required: true },
    defaultValue: "2019-09-06T13:07:04.054",
    description: "end date",
    control: {
      type: "date"
    }
  }
}
export default {
  title: "CV/Molecules/ExperienceListItem",
  components: { ExperienceListItem },
  argTypes: {
    ...argTypes
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <ExperienceListItem
    :company="company"
    :title="title"
    :about="about"
    :link="link"
    :location="location"
    :tasks="tasks"
    :startdate="startdate"
    :enddate="enddate"
  />
  `,
  components: { ExperienceListItem },
  props: Object.keys(argTypes)
})

export const ExperienceItem = Template.bind({})
