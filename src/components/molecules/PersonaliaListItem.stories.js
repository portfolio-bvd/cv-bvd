import PersonaliaListItem from "@/components/molecules/PersonaliaListItem"

export default {
  title: "CV/Molecules/PersonaliaListItem",
  components: { PersonaliaListItem },

  argTypes: {
    varwidth: {
      name: "width",
      control: {
        type: "select",
        options: ["200px", "250px", "300px", "400px", "500px"]
      }
    }
  }
}

const Template = (args, { argTypes }) => ({
  template: `
  <div :style="{width:varwidth}">
    <PersonaliaListItem
      :text="text"
      :icon="icon"
    />
  </div
  `,
  components: { PersonaliaListItem },
  props: Object.keys(argTypes)
})

export const Name = Template.bind({})
Name.args = {
  text: "Bastiaan Van denabeele",
  info: "name",
  icon: "person"
}

export const Nationality = Template.bind({})
Nationality.args = {
  text: "Belgian",
  info: "nationality",
  icon: "flag"
}

export const Living = Template.bind({})
Living.args = {
  text: "Barcelona",
  info: "living",
  icon: "home"
}

export const Email = Template.bind({})
Email.args = {
  text: "vandenabeele.bastiaan<wbr>@gmail.com",
  info: "email",
  icon: "email"
}
