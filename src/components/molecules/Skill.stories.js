import Skill from "@/components/molecules/Skill"

export default {
  title: "CV/Molecules/Skill",
  components: { Skill }
}

const Template = (args, { argTypes }) => ({
  template: `
  <div style="width: 200px">
    <skill
      :level="level"
      :name="name"
    />
  </div>
  `,
  components: { Skill },
  props: Object.keys(argTypes)
})

export const Main = Template.bind()
Main.args = {
  name: "Skill",
  level: 0.8
}
