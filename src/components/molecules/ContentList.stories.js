import ContentList from "@/components/molecules/ContentList"
import EducationListItem from "@/components/molecules/EducationListItem"
import { FullItemList } from "@/components/molecules/EducationListItem.stories.js"

export default {
  title: "CV/Molecules/ContentList",
  components: { ContentList },
  subcomponents: { EducationListItem }
}
const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ContentList, EducationListItem },
  template: `
  <ContentList :separator="separator">
    <EducationListItem
      v-for="item in items"
      :key="item.id"
      :degree="item.degree"
      :school="item.school"
      :location="item.location"
      :courses="item.courses"
      :startdate="item.startdate"
      :enddate="item.enddate"
    />
  </ContentList>
  `
})

export const ListMultiItems = Template.bind({})
ListMultiItems.args = {
  items: [
    {
      id: 1,
      degree: "Master of something",
      school: "University",
      location: "Neverland",
      courses: "fun",
      startdate: "2018-09-06T13:07:04.054",
      enddate: "2019-09-06T13:07:04.054"
    },
    {
      id: 2,
      degree: "Master of something",
      school: "University",
      location: "Neverland",
      startdate: "2018-10-06T13:07:04.054",
      enddate: "2019-12-06T13:07:04.054"
    }
  ],
  separator: true
}
