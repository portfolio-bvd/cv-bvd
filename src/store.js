import Vue from "vue"
import Vuex from "vuex"

import data from "./assets/resume/data.json"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    resume_data: data,
    A4Layout: {
      height: 1754,
      width: 1240,
      padding: 24
    }
  },
  getters: {
    // function for getting the variables data
    getresume: (state) => {
      return state.resume_data
    },
    getresumeEN: (state) => {
      return state.resume_data.cv_eng
    },
    getBaseUrl: (state) => {
      return state.baseUrl
    },
    getA4Layout: (state) => {
      return state.A4Layout
    }
  },

  mutations: {},
  actions: {}
})
