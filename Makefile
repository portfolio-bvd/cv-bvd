
dev:
	docker-compose -f docker-compose.development.yml up

dev-build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.development.yml build

exec:
	docker exec -it cv-bvd bash
