module.exports = {
  printWidth: 80,
  trailingComma: "none",
  tabWidth: 2,
  semi: false,
  singleQuote: false,
  vueIndentScriptAndStyle: false
}
