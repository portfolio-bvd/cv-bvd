import { addParameters, addDecorator } from "@storybook/vue"

import vuetify from "@/plugins/vuetify"

import "vuetify/dist/vuetify.min.css"

import { MINIMAL_VIEWPORTS, INITIAL_VIEWPORTS } from "@storybook/addon-viewport"

addParameters({
  viewport: {
    viewports: {
      ...MINIMAL_VIEWPORTS,
      ...INITIAL_VIEWPORTS
    }
  },
  layout: "centered"
})
addDecorator(() => ({
  vuetify,
  template: `
      <VApp id=storybook>
        <story/>
      </VApp>
      `
}))
